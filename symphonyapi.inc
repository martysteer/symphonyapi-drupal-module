<?php

function intializeSymphony(){	
	global $soapurl, $apikey, $client, $ns, $symphonyinitialised, $base_address;
	
	$base_address = "https://developer.sym-online.com";
	
	if($symphonyinitialised!==true){
		$ns = $base_address."/API/1_0/SOAP/";
		
		$soapurl = $ns."SymphonyAPI.asmx?WSDL";
		
		$apikey = variable_get("symphony_apikey", '');

		/* Initialize webservice using WSDL address */
		$client = new SoapClient($soapurl, array(
								'cache_wsdl' => WSDL_CACHE_NONE,  //Cache needs to be disabled when using multiple sites else PHP caches the old wdsl file
								'soap_version' => SOAP_1_1,
								'location' => $soapurl,
								'uri'      => $ns						
					));
		
		
		$symphonyinitialised = true;
	}
}

function CreateSignature($operation, $timestamp)
{
	$SecretKey = variable_get("symphony_secretkey", '');
	
	/* 
	 * PHP Conversion of CreateSignature method from http://developer.sym-online.com/API/1_0/help.aspx
	 */
	
	$data = utf8_encode($operation . $timestamp);
	
	$hash = hash_hmac('sha1', $data, utf8_encode($SecretKey), true);

	$signature = base64_encode($hash);

	return $signature;
}

function makeSOAPCall($functionname, $params){	

	global $ns, $client, $apikey, $symphonyinitialised;
	
	if(!$symphonyinitialised){
		intializeSymphony();
	}
	
	/*
	 * DateTime string needs to be of the format yyyy-MM-ddTHH:mm:ss.fffZ (eg. 2010-04-09T09:08Z).  PHP does not support milliseconds as a function, so the time needs to be calculated
	 * then the milliseconds concatenated onto the end as the microseconds divided by 1000.  The format 'P' date adds the timezone on as this is required even server->server if there is a different
	 * in timezones relating to BST since the SOAP server uses UTC only (and obviously is required if actually in different timezones).
	 */
	$t = microtime(true);
	$micro = sprintf("%03d",($t - floor($t)) * 1000000);
	$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	$milliseconds = floor($d->format("u")/1000);

	$isodate = $d->format("Y-m-d\TH:i:s");
	
	//Calculate offset from UTC
	$timezonemodifier = $d->format("P");
	
	$isodate .= ".".$milliseconds.$timezonemodifier;

	$signature = CreateSignature($functionname, $isodate);
	

	$soapParameters = array(
		'APIKey' => $apikey,
		'Timestamp' => $isodate,
		'Operation' => $functionname,
		'Signature' => $signature,
		'soapaction' => $ns.$functionname
	);

	$header = new SoapHeader($ns,'AuthenticationHeader',$soapParameters,false);
	$client->__setSoapHeaders($header);
	
	$result = $client->__soapCall($functionname, array($params));
	
	
	return $result;
}

/**
 * GetExtEventsDetails
 * The original method for returning event details, superseded by GetExtEventsDetails3
 * @param boolean $includepast Include or exclude events in the past
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetExtEventsDetails
 */
function GetExtEventsDetails($includePastEvents = true){
		
	$params = array("includePastEvents" => $includePastEvents,);
  $result = makeSOAPCall("GetExtEventsDetails", $params);
  $events = json_decode(json_encode($result), true);
  return $events;

}

/**
 * GetWebsitePageContent
 * Returns the website content field for an event
 * @param int $websiteId The website ID as defined in Symphony
 * @param string $friendlyUrlName The friendly URL name as defined in Symphony
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetWebsitePageContent
 */
function GetWebsitePageContent($websiteId, $friendlyUrlName){
	
	$params = array("websiteId" => $websiteId, "friendlyUrlName" => $friendlyUrlName , "isTest" => FALSE,);
  $result = makeSOAPCall("GetWebsitePageContent", $params);
  $content = json_decode(json_encode($result), true);
  return $content;
  
}


/**
 * GetProfileList
 * Returns the list of profiles out of Symphony.  This is is shown as the Area of Interest on the main selection list
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetProfileList
 */
function GetProfileList(){
	
	$params = array();
  $result = makeSOAPCall("GetProfileList", $params);
  $profiles = json_decode(json_encode($result), true);
  return $profiles;	
	
}  

/**
 * getEventDetails2
 * Returns the website content field for an event
 * @param int $eventId Event ID
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetEventDetails2
 */
function getEventDetails2($eventId){
	$params = array("eventId" => $eventId,);
  $result = makeSOAPCall("GetEventDetails2", $params);
  $event = json_decode(json_encode($result), true);
  return $event;

	
}

/**
 * GetClientCompanyList
 * Returns a list of Institutes
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetClientCompanyList
 */
function GetClientCompanyList(){
	
	$params = array();
  $result = makeSOAPCall("GetClientCompanyList", $params);
  $clients = json_decode(json_encode($result), true);
  return $clients;
}

/**
 * GetClientCompanyList
 * Returns a list of event types
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetEventTypeList
 */
function GetEventTypeList(){
	$params = array();
  $result = makeSOAPCall("GetEventTypeList", $params);
  $speakers = json_decode(json_encode($result), true);
  return $speakers;
}

/**
 * GetSpeakersforEvent
 * This method retrieves all speakers for an event
 * @param string $eventId Event ID
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetSpeakersforEvent
 */
function GetSpeakersforEvent($eventId){
  $params = array("eventId" => $eventId,);
  $result = makeSOAPCall("GetSpeakersforEvent", $params);
  $speakers = json_decode(json_encode($result), true);
  return $speakers;
}

/**
 * GetExtEventsDetails3
 * Returns a searchable set of events listed on the system sorted by date ascending
 * @param int $startDate Unix Timestamp.  If set to null, defaults to current date
 * @param int $endDate Unix timestamp
 * @param string $eventType See GetProfileList
 * @param string $eventTitle The title of the event to be searched for. 
 * @param string $subClientCompany See GetSubClientCompanyList
 * @param int $pageNumber must be greater than or equal to 1 otherwise everything is returned
 * @param int $resultCount Number of results per page
 * @param int[] $profileSearchArray Array of the IDs for the areas of interest to filte rby
 * @param series $series The series ID to filter by
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetExtEventsDetails3
 */
function GetExtEventsDetails3($clientCompanyId = null, $startDate = null, $endDate = null, $eventType = null, $eventTitle = null, $subClientCompany = null, $pageNumber = 1, $resultCount = 10, $profileSearchArray = null, $series = null){

	if($startDate==null){
		$formattedStart = substr(date("c"), 0, -6);
	}else{
		$formattedStart = substr(date("c", $startDate), 0, -6);
	}
	if($endDate==null){
		$formattedEnd = null;
	}else{
		$formattedEnd = substr(date("c", $endDate), 0, -6);
	}
	
	if(!is_array($profileSearchArray)){
		$profileSearchArray = null;
	}
	
	$params = array(
		"clientCompanyId" => $clientCompanyId,
		"startDate" => $formattedStart,
		"endDate" => $formattedEnd,
		"eventType" => $eventType,
		"eventTitle" => $eventTitle,
		"subClientCompany" => $subClientCompany,
		"pageNumber" => $pageNumber,
		"resultCount" => $resultCount,
		"profileSearchArray" => $profileSearchArray,
		"series" => $series,
	);
	
  $result = makeSOAPCall("GetExtEventsDetails3", $params);
  $events = json_decode(json_encode($result), true);
  return $events;
}

/**
 * GetSubClientCompanyList
 * Returns a list of sub Institutes
 * @see https://developer.sym-online.com/API/1_0/SOAP/SymphonyAPI.asmx?op=GetSubClientCompanyList
 */
function GetSubClientCompanyList(){
	
	$params = array();
  $result = makeSOAPCall("GetSubClientCompanyList", $params);
  $list = json_decode(json_encode($result), true);
  return $list;
  
}

/** getFormattedOutputError
 * Returns the error string from a SOAP Exception filtering for common API errors
 * @param Exception The Exception thrown
 */
function getFormattedOutputError($error){
	if(strpos($error, "Invalid API key")){
		return "The API Key has not been set correctly in the module configuration";
	}elseif(strpos($error, "Signature is incorrect")){
		return "One of either the API Key or the Secret Key has not been set correctly in the module configuration";
	}
	
	return "An error occured, please contact an administrator. $error";
}

/**
 * Some of the images stored on the amazon cloud have additional parameters which prevent them from loading. 
 * Running the links through this function removes the paramters allowing the images to load
 */
function removeAdditionalParams($link){
	//remove all additional parameters after the ?
	return current(explode("?", $link));
}
